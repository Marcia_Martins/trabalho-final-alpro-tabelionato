package tabelionato.pack;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class QueueLinked<E> implements QueueTAD<E>, Iterable<E> {
	
	private QueueLinked<Cliente> fila;//

	private static final class Node<E> {
		public E element;
		public Node<E> next;

		public Node(E e) {
			element = e;
			next = null;
		}
	}

	private Node<E> head;
	private Node<E> tail;
	private int count;

	public QueueLinked() {

		// fila = new QueueLinked<Cliente>();
		
		head = null;
		tail = null;
		count = 0;
	}

	@SuppressWarnings("unchecked")
	public E get() {
		count--;
		return (E) fila.dequeue();
	}

	
	public Node<E> getTail() {
		return tail;
	}

	public void setTail(Node<E> tail) {
		this.tail = tail;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setHead(Node<E> head) {
		this.head = head;
	}

	public int size() {
		return count;
	}

	public boolean isEmpty() {
		return (count == 0);
	}

	public void clear() {
		head = null;
		tail = null;
		count = 0;
	}

	public E remove() {
		if (isEmpty())
			throw new EmptyQueueException();
		Node<E> target = head;
		E item = target.element;
		head = target.next;
		target.element = null;
		target.next = null;
		if (head == null)
			tail = null;
		count--;
		return item;
	}

	public void add(E element) {
		Node<E> n = new Node<E>(element);
		if (head == null)
			head = n;
		else
			tail.next = n;
		tail = n;
		count++;
	}

	public E element() {
		if (isEmpty())
			throw new EmptyQueueException();
		return head.element;
	}

	public E getHead() throws EmptyQueueException {
		if (isEmpty())
			throw new EmptyQueueException();
		return head.element;
	}

	public E enqueue(E element) {
		Node<E> n = new Node<E>(element);
		if (head == null)
			head = n;
		else
			tail.next = n;
		tail = n;
		count++;
		return element;
	}

	public E dequeue() {
		if (isEmpty())
			throw new EmptyQueueException();
		Node<E> target = head;
		E item = target.element;
		head = target.next;
		target.element = null;
		target.next = null;
		if (head == null)
			tail = null;
		count--;
		return item;
	}

	public QueueLinked<Cliente> getFila() {
		return fila;
	}

	public Iterator<E> iterator() {
		return new Iterator<E>() {

			private Node<E> current = head;

			@Override
			public boolean hasNext() {

				return (current != null);
			}

			@Override
			public E next() {
				if (current == null) {
					throw new NoSuchElementException();
				}
				E item = current.element;
				return item;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("Not supported yet.");
			}
		};
	}

	public String toString() {
		StringBuilder s = new StringBuilder();
		Node<E> aux = head;
		for (int i = 0; i < count; i++) {
			s.append(aux.element.toString());
			s.append("\n");
			aux = aux.next;
		}
		return s.toString();
	}

	public E set(int i, Cliente value) {

		if ((i < 0) || (i >= count)) {
			throw new IndexOutOfBoundsException();
		}
		Node<E> aux = head;
		for (int j = 0; i < i; j++) {
			aux = aux.next;
		}
		E tmp = aux.element;
		aux.element = (E) value;
		return (tmp);

	}

}
