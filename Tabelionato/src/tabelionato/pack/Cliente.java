package tabelionato.pack;

import java.util.Random;
/** 
 * 
 * Classe Simulador
 * @version junho de 2014 
 * @author M�rcia Martins
 */

public class Cliente {
	private int numero; // numero do cliente
	private int instanteChegada;
	private int tempoAtendimento; // quantidade de tempo que resta para o cliente no caixa
	private static int total = 0;
	private static final Random gerador = new Random();
	public static int tempoMinAtendimento = 10;
	public static int tempoMaxAtendimento = 15;

	public Cliente(int n, int c) {
		numero = n;
		//numero = ++total;
		instanteChegada = c;
		tempoAtendimento = gerador.nextInt(tempoMaxAtendimento
				- tempoMinAtendimento + 1)
				+ tempoMinAtendimento; // gera valores entre 5 e 20
	}

	public int getNumero() {
		return numero;
	}

	public int getInstanteChegada() {
		return instanteChegada;
	}

	public void decrementarTempoAtendimento() {
		tempoAtendimento--;
	}

	public int getTempoAtendimento() {
		return tempoAtendimento;
	}
}
