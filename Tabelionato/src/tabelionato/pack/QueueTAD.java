package tabelionato.pack;

public interface QueueTAD<E> {
	int size();

	boolean isEmpty();

	void clear();

	E getHead();

	E enqueue(E element);

	E dequeue();

}
