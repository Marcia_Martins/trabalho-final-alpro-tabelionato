package simulador.pack;

import java.io.IOException;

import tabelionato.pack.Acumulador;
import tabelionato.pack.Cliente;
import tabelionato.pack.GeradorClientes;
import tabelionato.pack.QueueLinked;
import tabelionato.pack.QueueTAD;

/**
 * 
 * Classe Simulador
 * 
 * @version junho de 2014
 * @author M�rcia Martins
 * @param <E>Classe Gen�rica e Super
 */
public abstract class Simulador<E> {

	protected static int duracao = 2000;
	protected static double probabilidadeChegada = 0.3;
	protected QueueTAD<Cliente> fila;
	public GeradorClientes geradorClientes;
	protected Acumulador statTemposEsperaFila;
	protected Acumulador statComprimentosFila;
	protected boolean trace; // valor indica se a simulacao ira imprimir
								// passo-a-passo os resultados

	public Simulador(boolean t) {
		fila = new QueueLinked<Cliente>();
		geradorClientes = new GeradorClientes(probabilidadeChegada);
		statTemposEsperaFila = new Acumulador();
		statComprimentosFila = new Acumulador();
		trace = t;
	}

	public abstract void simular();

	public void limpar() {
		fila = new QueueLinked<Cliente>();
		geradorClientes = new GeradorClientes(probabilidadeChegada);
		statTemposEsperaFila = new Acumulador();
		statComprimentosFila = new Acumulador();
	}

	//public abstract String getLeArquivo() throws IOException;

	public void imprimirResultados() {

		System.out.println();
		System.out.println("***Resultados da Simulacao***");
		System.out.println("Duracao:" + duracao);
		System.out.println("Probabilidade de chegada de clientes:"
				+ probabilidadeChegada);
		System.out.println("Tempo de atendimento minimo:"
				+ Cliente.tempoMinAtendimento);
		System.out.println("Tempo de atendimento maximo:"
				+ Cliente.tempoMaxAtendimento);
		System.out.println("Clientes ainda na fila:" + fila.size());
		System.out.println("Total de clientes gerados:"
				+ geradorClientes.getQuantidadeGerada());
		System.out.println("Tempo medio de espera:"
				+ statTemposEsperaFila.getMedia());
		System.out.println("Comprimento medio da fila:"
				+ statComprimentosFila.getMedia());
	}
}
