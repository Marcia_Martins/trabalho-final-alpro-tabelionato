package simulador.pack;

import java.io.IOException;

import tabelionato.pack.Cliente;
import caixa.pack.Caixa;

/**
 * 
 * Classe SimuladorSupermercado
 * 
 * @version junho de 2014
 * @author M�rcia Martins
 */
public class SimuladorSupermercado extends Simulador {
	private Caixa[] caixas;
	private static final int totalCaixas = 10;

	public SimuladorSupermercado(boolean t) {
		super(t);
		caixas = new Caixa[totalCaixas];
		for (int c = 0; c < totalCaixas; c++)
			caixas[c] = new Caixa();// (c + 1);

	}

	@Override
	public void simular() {
		// realizar a simulacao por um certo numero de passos de duracao
		for (int tempo = 0; tempo < super.duracao; tempo++) {
			// verificar se um cliente chegou
			if (geradorClientes.gerar()) {
				// se cliente chegou, criar um cliente e inserir na fila do
				// caixa
				Cliente c = new Cliente(geradorClientes.getQuantidadeGerada(),
						tempo);
				fila.enqueue(c);
				if (super.trace)
					System.out.println(tempo + ": cliente " + c.getNumero()
							+ " (" + c.getTempoAtendimento()
							+ " min) entra na fila - " + fila.size()
							+ " pessoa(s)");
			}
			// verificar se os caixas estão vazios
			for (int c = 0; c < totalCaixas; c++) {
				if (caixas[c].estaVazio()) {
					// se o caixa esta vazio, atender o primeiro cliente da fila
					// se ele existir
					if (!fila.isEmpty()) {
						// tirar o cliente do inicio da fila e atender no caixa
						// caixas[c].atenderNovoCliente(fila.enqueue(c));
						statTemposEsperaFila.adicionar(tempo
								- caixas[c].getClienteAtual()
										.getInstanteChegada());
						if (trace)
							System.out.println(tempo + ": cliente "
									+ caixas[c].getClienteAtual().getNumero()
									+ " chega ao caixa " + (c + 1));
					}
				} else {
					// se o caixa ja esta ocupado, diminuir de um em um o tempo
					// de atendimento ate chegar a zero
					if (caixas[c].getClienteAtual().getTempoAtendimento() == 0) {
						if (trace)
							System.out.println(tempo + ": cliente "
									+ caixas[c].getClienteAtual().getNumero()
									+ " deixa o caixa " + (c + 1));
						caixas[c].dispensarClienteAtual();
					} else {
						caixas[c].getClienteAtual()
								.decrementarTempoAtendimento();
					}
				}
			}
			statComprimentosFila.adicionar(fila.size());
		}
	}

	public int totalCaixa() {
		// TODO Auto-generated method stub
		return 0;

	}

	@Override
	public void limpar() {

	}

	@Override
	public String toString() {
		return super.toString();
	}


}
