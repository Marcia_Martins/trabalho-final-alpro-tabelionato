package simulador.pack;

import tabelionato.pack.Acumulador;
import tabelionato.pack.Cliente;
import tabelionato.pack.GeradorClientes;
import tabelionato.pack.QueueLinked;
import caixa.pack.CaixaTabelionato;

public class SimuladorTabelionato extends Simulador
{
   
    private CaixaTabelionato caixa;
    
    
    public SimuladorTabelionato(boolean t) {
    	super(t);
        caixa = new CaixaTabelionato();
        geradorClientes = new GeradorClientes(probabilidadeChegada);
        
    }
    
    public void simular()
    {
        //realizar a simulacao por um certo numero de passos de duracao
        for(int tempo=0; tempo<duracao; tempo++)
        {
            //verificar se um cliente chegou
            if(geradorClientes.gerar())
            {
                //se cliente chegou, criar um cliente e inserir na fila do caixa
                Cliente c = new Cliente(geradorClientes.getQuantidadeGerada(),tempo);
                fila.enqueue(c);
                if(trace)
                    System.out.println(tempo + ": cliente " + c.getNumero() + " ("+c.getTempoAtendimento()+" min) entra na fila - " + fila.size() + " cliente(s)");
            }
            //verificar se o caixa esta vazio
            if(caixa.estaVazio())
            {
                //se o caixa esta vazio, atender o primeiro cliente da fila se ele existir
                if(!fila.isEmpty())
                {
                    //tirar o cliente do inicio da fila e atender no caixa
                    caixa.atenderNovoCliente((Cliente) fila.dequeue());
                    statTemposEsperaFila.adicionar(tempo - caixa.getClienteAtual().getInstanteChegada());
                    if(trace)
                        System.out.println(tempo + ": cliente " + caixa.getClienteAtual().getNumero() + " chega ao caixa.");
                }
            }
            else
            {
                //se o caixa ja esta ocupado, diminuir de um em um o tempo de atendimento ate chegar a zero
                if(caixa.getClienteAtual().getTempoAtendimento() == 0)
                {
                    if(trace)
                        System.out.println(tempo + ": cliente " + caixa.getClienteAtual().getNumero() + " deixa o caixa.");
                    caixa.dispensarClienteAtual();
                }
                else
                {
                    caixa.getClienteAtual().decrementarTempoAtendimento();
                }
            }
            statComprimentosFila.adicionar(fila.size());
        }
    }
    
    public void limpar()
    {
        super.limpar();
        caixa = new CaixaTabelionato();
        
    }
    
    public void imprimirResultados()
    {
      super.imprimirResultados();
        System.out.println("Cliente atendidos:" + caixa.getNumeroAtendidos());
        System.out.println("Cliente ainda no caixa:" + (caixa.getClienteAtual() != null));
       
    }
}
