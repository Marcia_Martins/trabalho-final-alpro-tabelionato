package caixa.pack;

import tabelionato.pack.Cliente;

/**
 * 
 * Classe SimuladorSupermercado
 * 
 * @version junho de 2014
 * @author M�rcia Martins
 */
public class CaixaSupermercado extends Caixa {
	private int numero;

	public CaixaSupermercado(int num) {
		super();
		numero = num;
	}

	public int getNumero() {
		return numero;
	}

	public void atenderNovoCliente(Cliente c) {
		super.atenderNovoCliente(c);
	}

	public Cliente dispensarClienteAtual() {
		return super.dispensarClienteAtual();
	}

	public boolean estaVazio() {
		return super.estaVazio();
	}

	public Cliente getClienteAtual() {
		return super.getClienteAtual();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CaixaSupermercado [numero=");
		builder.append(numero);
		builder.append(", getNumero()=");
		builder.append(getNumero());
		builder.append(", dispensarClienteAtual()=");
		builder.append(dispensarClienteAtual());
		builder.append(", estaVazio()=");
		builder.append(estaVazio());
		builder.append(", getClienteAtual()=");
		builder.append(getClienteAtual());
		builder.append("]");
		return builder.toString();
	}

}
